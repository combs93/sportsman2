/* frameworks */
//=include ../../node_modules/jquery/dist/jquery.min.js
//=include ../../node_modules/swiper/js/swiper.min.js
//=include ../../node_modules/shufflejs/dist/shuffle.min.js

/* libs */
//=include lib/modernizr-custom.js
//=include lib/jquery.scrollbar.min.js
//=include lib/resize-sensor.js

/* plugins */
//=include ../../node_modules/popper.js/dist/umd/popper.min.js
//=include ../../node_modules/bootstrap/js/dist/util.js
//=include ../../node_modules/bootstrap/js/dist/dropdown.js
//=include ../../node_modules/bootstrap/js/dist/collapse.js
//=include ../../node_modules/jquery-validation/dist/jquery.validate.min.js
//=include ../../node_modules/autosize/dist/autosize.min.js
//=include ../../node_modules/svg4everybody/dist/svg4everybody.min.js
//=include ../../node_modules/select2/dist/js/select2.min.js
//=include ../../node_modules/simplebar/dist/simplebar.min.js
//=include ../../node_modules/sticky-sidebar/dist/sticky-sidebar.min.js
//=include ../../node_modules/ion-rangeslider/js/ion.rangeSlider.min.js

/* separate */
//=include helpers/object-fit.js
//=include helpers/valid.js
//=include separate/global.js
//=include helpers/bs_modal_fix.js
//=include helpers/bsModalCenter.js
//=include helpers/modal-scroll-fix.js
//=include plugins/bs/modal.js

/* components */
//=include components/js-header.js
//=include components/js-good.js
//=include components/js-form.js
//=include components/js-tabs.js
//=include components/js-collpase.js
//=include components/js-select.js
//=include components/js-order.js
//=include components/js-cart.js
//=include components/js-livesearch.js
//=include components/js-chat.js
//=include components/js-coins.js
//=include components/js-withdraw.js
//=include components/js-invoice.js
//=include components/js-sidebar.js
//=include components/js-sticky-helpers.js
//=include components/js-products-filters.js
//=include components/js-range-slider.js
//=include components/js-feedbacks.js
//=include components/js-payments.js
//=include components/js-carousel.js
//=include components/js-settings.js
//=include components/js-modal-auth.js
//=include components/js-countdown.js
//=include components/js-checkout.js
//=include components/js-copy-btn.js

// the main code
svg4everybody();

// if modalWindow is visible - make page noscrollable 
let scrlTop;

$(window).on('scroll', function() {
    scrlTop = `${window.scrollY}px`;
});

const showDialog = function () {
    const body = document.body;
    let sizeChecker = document.createElement('div');
    let scrollWidth;

    
    sizeChecker.style.overflowY = 'scroll';
    sizeChecker.style.width = '50px';
    sizeChecker.style.height = '50px';
    body.append(sizeChecker);
    scrollWidth = sizeChecker.offsetWidth - sizeChecker.clientWidth;
    
    sizeChecker.remove();
    
    body.style.position = 'fixed';
    body.style.width = '100%';
    body.style.height = '100%';
    body.style.top = `-${scrlTop}`;
    body.style.overflow = 'hidden';
    body.style.paddingRight = scrollWidth + 'px';
};
const closeDialog = function () {
    let body = document.body;
    let scrollY = body.style.top;
    body.style.position = '';
    body.style.top = '';
    body.style.width = '';
    body.style.overflow = '';
    body.style.paddingRight = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
}