$(document).ready(function() {

    $('.modal').on('show.bs.modal', function (event) {
        $('body').addClass('modal-active');
        showDialog();
    });

    $('.modal').on('shown.bs.modal', function (event) {
        $('body').removeClass('modal-open');
    });

    $('.modal').on('hide.bs.modal', function (event) {
        $('body').removeClass('modal-active');
        closeDialog();
    });
    
});