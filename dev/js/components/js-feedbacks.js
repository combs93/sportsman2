(function () {
    if ($('.js-shuffle-container').length) {
        const Shuffle = window.Shuffle;
        const shuffleSettings = {
            itemSelector: '.feedbacks-col',
            sizer: '.my-sizer-element',
            easing: 'ease',
            staggerAmountMax: 0,
            staggerAmount: 0,
            speed: 300
        };

        let shuffleInstance = new Shuffle(document.querySelector('.js-shuffle-container'), shuffleSettings);

        $('.js-shuffle-trigger').on('click', function() {
            if ($('.js-shuffle-container').hasClass('shuffle')) {
                const $this = $(this);
                const keyword = $this.attr('data-group');

                shuffleSettings.speed = 500;
                shuffleInstance.update();
                
                $this.siblings('.js-shuffle-trigger.is-active').removeClass('is-active');
                $this.addClass('is-active');
                shuffleInstance.filter(keyword);
            }
        });


        $('.feedbacks-item').each(function(i,item) {
            let maxHeight = $(this).find('.feedbacks-item__info p').outerHeight() + 20;
            $(this).find('.feedbacks-item__info').attr('data-height', maxHeight);
        });
    
        $('.feedbacks-item__info__more').on('click', function () {
            const $parent = $(this).closest('.feedbacks-item__info');
            if ($parent.hasClass('is-opened')) {
                $parent.removeClass('is-opened');
                $parent.css('max-height', 40);
            } else {
                $parent.addClass('is-opened');
                $parent.css('max-height', $parent.data('height'));
            }
            let $interval = setInterval(() => {
                shuffleInstance.update();
            }, 10);
            setTimeout(() => {
                clearInterval($interval);
            }, 500);
        });

        $('.js-feedbacks-filters-trigger').on('click', function() {
            $(this).closest('.feedbacks-wrapper').find('.feedbacks-aside').fadeToggle(200);
        });
    }

    $('.js-feedback-like').on('click', function() {
        const $this = $(this);
        const $likesNum = $this.closest('.feedbacks-item__actions').find('.feedback_likes_count');

        $this.addClass('is-active');
        $likesNum.text(+$likesNum.text() + 1);
    });
}());