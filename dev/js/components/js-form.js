(function () {
    $('.form-control--simple, .form-control--dark').on('input', function() {
        const $this = $(this);
        $this.val().length > 0 ? $this.addClass('is-filled') : $this.removeClass('is-filled') 
    });

    // feedback modal form
    $('.feedback-modal__form').on('submit', function() {
        if ($(this).find('.input-wrap').hasClass('error')) {
            return
        }
        $('#feedback_modal').modal('hide');
        $('#feedback_done_modal').modal('show');
    });

    // textarea resize height on input
    autosize($('textarea'));

    // support textarea
    $('#support_textarea, #comments_textarea').on('focusout', function() {
        const $this = $(this);
        if ($this.val().length > 0) {
            $this.addClass('is-filled');
        } else {
            $this.removeClass('is-filled');
        }
    });
}());