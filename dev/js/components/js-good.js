(function () {
    // decrease
    $('.goods-item__count__decrease').on('click', function () {
        const $input = $(this).closest('.goods-item__count').find('.goods-item__count__input');
        let $count = parseInt($input.val()) - 1;
        $input.val($count);
        $input.change();
    });
    
    // increase
    $('.goods-item__count__increase').on('click', function () {
        const $input = $(this).closest('.goods-item__count').find('.goods-item__count__input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
    });

    $('.my-cart-table-row').on('click', '.goods-item__count__btn', function() {
        const $this = $(this);
        const $parentRow = $this.closest('.my-cart-table-row');

        $this.siblings('.js-num-input').val() > 0 ? $parentRow.addClass('is-filled') : $parentRow.removeClass('is-filled');
    });
    
    $('.js-num-input').on('keypress input change', function(){
        const $this = $(this);
        let $val = $this.val();
        let $count = parseInt($val);
    
        $count = ($count < 0 || isNaN($count)) ? 0 : $count;
        $this.val($count);
    });
    
    $('.js-out-of-stock-noty').on('click', function() {
        const $this = $(this);
        if ($this.hasClass('is-active')) {
            return;
        }
        $this.addClass('is-active');
        $this.find('.product-block__submit__noty').hide();
        $this.find('.product-block__submit__noty-receive').fadeIn(300);
    });

    $('.js-add-to-cart').on('click', function() {
        const $this = $(this);
        const $productId = $this.closest('.js-product').attr('id')
        const $productCount = $this.closest('.js-product').find('.goods-item__count__input').val();
        $this.addClass('is-adding')
        $this.attr('disabled', '');
        setTimeout(() => {
            $this.removeClass('is-adding');
            $this.addClass('is-added');
            $this.removeAttr('disabled');
        }, 2500);
        setTimeout(() => {
            $this.closest('.product-block').removeClass('is-flipped').addClass('product-block--in-cart');
        }, 3200);

        addProductToStickyCart($productId, $productCount);
    });

    function setDiscountPrice(item, count) {
        const $this = item;
        const $productBlock = $this.closest('.js-product');
        const $currentPrice = +$this.closest('.js-product').find('.price-current').text();
        
        const $discountPrice = count == 5 ? $currentPrice * 0.9 : $currentPrice * 0.8;
        $productBlock.addClass('is-discount');
        $productBlock.find('.price-discount').text($discountPrice.toFixed(2))
    }
    
    
    $('.label-radio-rounded__input').on('change', function() {
        const $count = +$(this).siblings('input[type="hidden"]').val();
        $(this).closest('.js-product').find('.goods-item__count__input').val($count);
        setDiscountPrice($(this), $count);
    });
    
    $('.js-product').on('change input', '.goods-item__count__input', function() {
        const $this = $(this);
        const $parentBlock = $this.closest('.js-product');
        const $value = $this.val();
        
        if (!$(this).closest('.my-cart-table.product-list-table').length) {
            if ($value < 1) {
                return $this.val(1)
            }
        }

        $parentBlock.find('.label-radio-rounded input[type="hidden"]').each(function(i, item) {
            if ($value > 4 && $value < 10) {
                setDiscountPrice($this, 5);
                if ($(this).val() == 5) {
                    $(this).closest('.product-detail__discount__line').siblings('.product-detail__discount__line').find('.label-radio-rounded__input').prop('checked', false);
                    $(this).siblings('.label-radio-rounded__input').prop('checked', true);
                }
            } else if ($value > 9) {
                setDiscountPrice($this, 10);
                if ($(this).val() == 10) {
                    $(this).closest('.product-detail__discount__line').siblings('.product-detail__discount__line').find('.label-radio-rounded__input').prop('checked', false);
                    $(this).siblings('.label-radio-rounded__input').prop('checked', true);
                }
            } else if ($value < 5) {
                $parentBlock.removeClass('is-discount');
                $(this).closest('.product-detail__discount__line').find('.label-radio-rounded__input').prop('checked', false);
            }
        });
    });

    $('.products-inner__content__side.side-list').on('change input', '.goods-item__count__input', function() {
        const $this = $(this);
        let $count = $this.val();
        const $id = $this.closest('.my-cart-table-row').attr('id');
        const $parentRow = $this.closest('.my-cart-table-row').find('.my-cart-table__total');
        let $promoVal = $parentRow.find('.my-cart-table__total__promo__val');

        if ($count < 1) {
            $('.sticky-helpers').find('.cart-sticky__inner__products__item[data-id="'+$id+'"]').remove();
            let $productsLength = $('.sticky-helpers').find('.cart-sticky__inner__products__item').length;
            $('.sticky-helpers').find('.sticky-helpers__count, .count-box').text($productsLength);
            return;
        }
        
        $count > 4 ? $parentRow.addClass('is-promo-active') : $parentRow.removeClass('is-promo-active');
        
        if ($count > 4 && $count < 10) {
            $promoVal.text('5%')
        } else if ($count > 9) {
            $promoVal.text('10%')
        }

        addProductToStickyCart($id, $count);
    });

    function addProductToStickyCart(productId, productCount) {
        const $parent = $('.cart-sticky__inner__products');
        let $isInCart = false;
        $('.sticky-helpers').addClass('is-visible');
        
        // product structure
        let $product = '<div class="cart-sticky__inner__products__item" data-id="'+ productId +'">';
        $product += '<div class="my-cart-table__product__img">';
        $product += '<img src="s/images/useful/products/sticky-card-product.jpg" alt="product"/>';
        $product += '</div>';
        $product += '<div class="my-cart-table__product__text">';
        $product += '<div class="my-cart-table__product__text__main">';
        $product += '<div class="product-indicator product-indicator--sky"></div>';
        $product += '<span class="my-cart-table__product__name">Mesterolone</span>';
        $product += '</div>';
        $product += '<div class="my-cart-table__product__text__secondary"><span>130.00 USD</span></div>';
        $product += '</div>';
        $product += '<div class="my-cart-table__product__side">';
        $product += '<div class="my-cart-table__product__count">'+ productCount +'</div>';
        $product += '<button type="button" class="my-cart-table__product__remove js-sticky-product-remove">';
        $product += '<svg width="1em" height="1em" class="icon icon-bin ">';
        $product += '<use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-bin"></use>';
        $product += '</svg>';
        $product += '</button>';
        $product += '</div>';
        $product += '</div>';

        if ($parent.find('.cart-sticky__inner__products__item').length) {
            $parent.find('.cart-sticky__inner__products__item').each(function(i, item) {
                const $id = $(this).attr('data-id');
                if (productId === $id) {
                    $isInCart = true;
                }
            });
        }

        if ($isInCart) {
            $parent.find('.cart-sticky__inner__products__item').each(function(i, item) {
                if ($(this).attr('data-id') == productId) {
                    $(this).find('.my-cart-table__product__count').text(productCount)
                }
            });
        } else {
            $parent.append($product);
        }

        let $productsLength = $parent.find('.cart-sticky__inner__products__item').length;
        $('.sticky-helpers').find('.sticky-helpers__count, .count-box').text($productsLength);
    }
    
    $(window).on('load', function() {
        $('.my-cart-table-row').each(function(i, item) {
            const $this = $(this);
            const $inputVal = $this.find('.goods-item__count__input').val();
            const $totalCol = $this.find('.my-cart-table__total');
            const $promoVal = $this.find('.my-cart-table__total__promo__val');

            if ($inputVal > 4) {
                $totalCol.addClass('is-promo-active');
            }

            if ($inputVal > 4) {
                $totalCol.addClass('is-promo-active');
            }

            if ($inputVal > 4 && $inputVal < 10) {
                $promoVal.text('5%')
            } else if ($inputVal > 9) {
                $promoVal.text('10%')
            }
        })
    });
}());