(function () {
    $('.js-search-input').on('input', debounce(function() {
        const $this = $(this);
        let $searchField = $this.val();
        let $expression = new RegExp($searchField, 'i');
        
        $('.header__search__list').html('');
        if ($searchField.length > 0) {
            $this.closest('.header').addClass('search-active');
            $('.header-search-backdrop').addClass('is-active');
            $.getJSON('s/data/search.json', function(data) {
                $.each(data, function(key, value) {
                    if (value.name.search($expression) != -1) {
                        let inner = '<div class="header__search__list__item">';
                        inner += '<a href="javascript:void(0)">';
                        inner += value.name;
                        inner += '</a>';
                        inner += '</div>';
                        $('.header__search__list').append(inner);
                    }
                });
            });
            if ($(window).width() < 1280) {
                showDialog();
            }
            setMobileSearchListStyles();
        } else {
            $('.header__search__list').html('');
            $this.closest('.header').removeClass('search-active');
            $('.header-search-backdrop').removeClass('is-active');
            if ($(window).width() < 1280) {
                closeDialog();
            }
        }
    }, 300));

    $('.js-search-close').on('click', function(e) {
        $('.js-search-close.is-active').removeClass('is-active');
        $('.header__mobile__search.is-active').removeClass('is-active');
        $('.header__search__list').html('');
        $('.header__search').removeClass('is-active is-focused');
        $('.header').removeClass('search-active');
        $('.js-search-input').val('')
        if ($(window).width() < 1280) {
            closeDialog();
        }
    });

    $('.js-search-trigger').on('click', function() {
        $(this).closest('.header__mobile__search').addClass('is-active');
    });

    $('.header__mobile__search').on('focus', '.js-search-input', function() {
        $(this).closest('.header__search').addClass('is-focused');
    });
    $('.header__mobile__search').on('focusout', '.js-search-input', function() {
        if ($(this).val().length < 1) {
            $(this).closest('.header__search').removeClass('is-focused');
        }
    });

    $('body').on('click', function(e){
        if ($(e.target).closest('.header__mobile__search').length === 0 && $('.header__mobile__search').find('.js-search-input').val().length < 1) {
            $('.header__mobile__search.is-active').removeClass('is-active');
        }
    });

    function setMobileSearchListStyles() {
        if ($(window).width() < 1280 && $('.header').hasClass('header--noty-active')) {
            $('.header__search__list-box--mobile').css('top', $('.header__noty').outerHeight() + $('.header__mobile__main').outerHeight());
        }
    }

    $('.js-noty-close').on('click', function() {
        if ($(window).width() < 1280) {
            $('.header__search__list-box--mobile').css('top', $('.header__mobile__main').outerHeight());
        }
    });

    $(window).on('resize', setMobileSearchListStyles);
}());