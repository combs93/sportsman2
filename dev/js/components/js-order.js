(function () {
    // collase order block 
    $('.js-order-trigger').on('click', function() {
        const $this = $(this);
        const $parent = $this.closest('.order-block')

        if ($parent.hasClass('is-collapsed')) {
            $parent.removeClass('is-collapsed');
            $parent.find('.order-block__content').stop().slideUp(300);
            $parent.find('.order-block__side .order-block__btn--hidden').fadeOut(200);
            setTimeout(() => {
                $parent.find('.order-block__side .order-block__btn--hide').fadeIn(200);
            }, 200)
        } else {
            $parent.addClass('is-collapsed');
            $parent.find('.order-block__content').stop().slideDown(300);
            $parent.find('.order-block__side .order-block__btn--hidden').fadeIn(200);
            $parent.find('.order-block__side .order-block__btn--hide').hide();
        }

    });

    // open order modal with question
    function openOrderModal(btn) {
        const $parent = btn.closest('.order-block');
        
        if (!$parent.hasClass('is-collapsed')) {
            $parent.toggleClass('is-collapsed');
            $parent.find('.order-block__content').stop().slideToggle(300);
            setTimeout(() => {
                $parent.find('.order-block__modal').addClass('is-active');
            }, 500);
        } else {
            $parent.find('.order-block__modal').addClass('is-active');
        }
    }
    
    $('.js-order-receive').on('click', function() {
        openOrderModal($(this));
    });
    
    $('.js-confirm-receive').on('click', function() {
        const $this = $(this);
        $this.closest('.order-block').addClass('order-block--received');
        $this.closest('.order-block__modal ').removeClass('is-active');
    });
    
    $('.js-order-delete').on('click', function() {
        openOrderModal($(this));
    });
    
    $('.js-confirm-delete').on('click', function() {
        $(this).closest('.order-block').remove();
    });

    $('.js-order-modal-close').on('click', function() {
        $(this).closest('.order-block__modal').removeClass('is-active');
    });
}());