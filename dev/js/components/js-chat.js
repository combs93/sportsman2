(function () {
    const supportScrollInstance = $('.acc-support__body');
    if (supportScrollInstance.length) {
        // if (!Modernizr.touchevents) {
        // }
        let simpleBar = new SimpleBar(supportScrollInstance[0]);
        
        let itemsPerPage = 4;
        let messagesShown = 0;
        let messagesLoading = false;
        let lastScroll = 0;
        let allMessagesLoaded = false;
        let loader = $('.loader-wrapper')

        function renderMessage(value) {
            let sctructure = '<div class="acc-support__message ' + value.modifier + '">';
            sctructure += '<div class="acc-support__message__inner">';
            sctructure += '<p>'
            sctructure += value.message
            sctructure += '</p>'
            sctructure += '<span class="acc-support__message__time">'
            sctructure += value.date
            sctructure += '<img class="icon-chk" src="s/images/useful/svg/check.svg", alt="check"/>'
            sctructure += '</span>'
            sctructure += '</div>';
            sctructure += '</div>';
            return sctructure;
        }
        
        function loadMessages(loadItems) {
            $.ajax({
                url: 's/data/chat.json',
                dataType: 'json',
                beforeSend: function() {
                    lastScroll = simpleBar.getScrollElement().scrollHeight;
                    messagesLoading = true;
                    loader.addClass('is-active');
                }
            })
            .done(debounce(function (data) {
                const messages = data.slice(-messagesShown - loadItems, -messagesShown || data.length);
        
                if (data.length > messagesShown) {
                    let html = '';
                    $.each(messages, function (key, value) { html += renderMessage(value);})
                    $(html).insertAfter(loader);
                    loader.removeClass('is-active');
                    supportScrollInstance.scrollTop(supportScrollInstance.scrollHeight - lastScroll);
                    messagesShown += loadItems;
                } else {
                    allMessagesLoaded = true;
                }
        
                loader.removeClass('is-active');

                simpleBar.getScrollElement().scrollTop = simpleBar.getScrollElement().scrollTop + 200
        
                messagesLoading = false;
            }, 1000))
            .fail(function () {
                loader.addClass('is-active');
            });
        }

        $(document).ready(function() {
            loadMessages(10);
            setTimeout(() => {
                simpleBar.getScrollElement().scrollTo(0, $('.acc-support__inner').outerHeight());
            }, 1100);
        });

        simpleBar.getScrollElement().addEventListener('scroll', function(){
            if (simpleBar.getScrollElement().scrollTop < 20 && !messagesLoading && !allMessagesLoaded) {
                loadMessages(4);
            }
        });
    }

}());