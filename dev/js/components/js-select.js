(function () {
    $('.js-select').each(function() {
        const $placeholder = $(this).data('placeholder');
        $('.js-select').select2({
            minimumResultsForSearch: -1,
            placeholder: $placeholder
        });
    });

    $('.js-filters-select').each(function() {
        const $placeholder = $(this).data('placeholder');
        $('.js-filters-select').select2({
            minimumResultsForSearch: -1,
            placeholder: $placeholder
        });
    });
}());