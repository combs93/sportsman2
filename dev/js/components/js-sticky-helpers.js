

(function () {
    let $stickyHelpersSimpleBar;

    $(window).on('load', function() {
        countStickyProducts();
    });

    $('.js-sticky-helper-trigger').on('click', function() {
        const $this = $(this);
        const $stickyBlock = $this.siblings('.js-sticky-helper-inner');
        const $simpleBarInstance = $this.siblings('.js-sticky-helper-inner').find('.js-simplebar-instance');
        
        if ($this.hasClass('is-active')) {
            $stickyBlock.stop().fadeOut(200).removeClass('is-opened');
            $this.removeClass('is-active');
            $this.closest('.sticky-helpers').find('.js-sticky-helper-trigger.is-disabled').removeClass('is-disabled');
            $('body').removeClass('header-dropdown-open');
            setTimeout(() => {
                $this.closest('.sticky-helpers').removeClass('is-opened');
                $this.closest('.sticky-block').removeClass('is-active');
            }, 250);
            
            if ($(window).width() < 768) {
                closeDialog();
            }
        } else {
            $stickyBlock.stop().fadeIn(200).addClass('is-opened');
            $this.addClass('is-active');
            $this.closest('.sticky-helpers').find('.js-sticky-helper-trigger:not(.is-active)').addClass('is-disabled');
            $this.closest('.sticky-helpers').addClass('is-opened');
            $this.closest('.sticky-block').addClass('is-active');
            
            if ($(window).width() < 768) {
                $('body').addClass('header-dropdown-open');
                showDialog();
            }
        }

        if ($simpleBarInstance.length) {
            $stickyHelpersSimpleBar = new SimpleBar($simpleBarInstance[0]);
        }

        if ($this.siblings('.js-sticky-helper-inner').find('.support-sticky__inner__messages').length) {
            $stickyHelpersSimpleBar.getScrollElement().scrollTo(0, $('.support-sticky__inner__messages').find('.acc-support__inner').outerHeight());
        }        
    });    
    
    $('.js-sticky-helper-close').on('click', function() {
        const $this = $(this);
        const $stickyBlock = $this.closest('.js-sticky-helper-inner');
        const $stickyBlockTrigger = $this.closest('.js-sticky-helper-inner').siblings('.js-sticky-helper-trigger');
        const $simpleBarInstance = $this.closest('.js-sticky-helper-inner').find('.js-simplebar-instance');
        
        $stickyBlock.fadeOut(200).removeClass('is-opened');
        $stickyBlockTrigger.removeClass('is-active');
        $this.closest('.sticky-helpers').find('.js-sticky-helper-trigger.is-disabled').removeClass('is-disabled');
        $('body').removeClass('header-dropdown-open');
        setTimeout(() => {
            $this.closest('.sticky-helpers').removeClass('is-opened');
            $this.closest('.sticky-block').removeClass('is-active');
        }, 250);
        
        if ($(window).width() < 768) {
            closeDialog();
        }

        if ($this.closest('#support_request_form').length) {
            $this.closest('#support_request_form').removeClass('is-done-request');
            $this.closest('#support_request_form').find('.support-sticky__inner__info').show();
            $this.closest('#support_request_form').find('.support-sticky__inner__form').show();
        }
    });

    $('.sticky-helpers').on('click', '.js-sticky-product-remove', function() {
        const $this = $(this);
        const $id = $this.closest('.cart-sticky__inner__products__item').attr('data-id');
        const $productInList = $('body').find('#'+$id); 
        
        $this.closest('.cart-sticky__inner__products__item').remove();

        if ($productInList.closest('.my-cart-table.product-list-table').length) {
            $productInList.removeClass('is-filled');
            $productInList.find('.goods-item__count__input').val(0);
            $productInList.find('.my-cart-table__total').removeClass('is-promo-active');
        } else {
            $productInList.removeClass('product-block--in-cart');
            $productInList.find('.js-add-to-cart').removeClass('is-added');
        }

        countStickyProducts();
    }); 

    function countStickyProducts() {
        let $productsLength = $('.cart-sticky__inner__products').find('.cart-sticky__inner__products__item').length;
        $('.cart-sticky.sticky-block').find('.sticky-helpers__count, .count-box').text($productsLength);
    }


    $('#support_request_form').on('submit', function() {
        // if success
        $(this).find('.support-sticky__inner__info').fadeOut(300);
        $(this).find('.support-sticky__inner__form').fadeOut(300);
        setTimeout(() => {
            $(this).addClass('is-done-request');
        }, 300);
    });
    
    $('.js-request-done-close').on('click', function() {
        const $this = $(this);
        
        $this.closest('.js-sticky-helper-inner').slideUp(300).removeClass('is-opened');
        $this.closest('#support_request_form').removeClass('is-done-request');
        $(this).closest('#support_request_form').find('.support-sticky__inner__info').show();
        $(this).closest('#support_request_form').find('.support-sticky__inner__form').show();
        $this.closest('.sticky-helpers').find('.js-sticky-helper-trigger.is-disabled').removeClass('is-disabled');

        if ($(window).width() < 768) {
            closeDialog();
        }
    });
}());