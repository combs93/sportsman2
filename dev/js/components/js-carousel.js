(function () {
    if ($('.product-carousel').length) {
        $('.product-carousel').each(function(i, item) {
            const swiper = new Swiper($(item).find('.swiper-container'), {
                direction: 'horizontal',
                slidesPerView: 'auto',
                slidesPerGroup: 1,
                speed: 800,
                loop: false,
                spaceBetween: 15,
                centeredSlides: false,
                slidesOffsetBefore: 0,
                pagination: {
                    el: $(this).find('.product-carousel__pagination'),
                    clickable: true,
                },
                navigation: {
                    nextEl: $(this).find('.product-carousel__navigation__right'),
                    prevEl: $(this).find('.product-carousel__navigation__left'),
                },
                breakpoints: {
                    768: {
                        spaceBetween: 20,
                    },
                    1024: {
                        spaceBetween: 25,
                    },
                    1280: {
                        spaceBetween: 30,
                    },
                    1600: {
                        spaceBetween: 40,
                    }
                },
            });
        });
    }

    if ($('.main-banner__carousel').length) {
        const swiper = new Swiper($('.main-banner__carousel').find('.swiper-container'), {
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            centeredSlides: false,
            slidesPerGroup: 1,
            spaceBetween: 10,
            speed: 800,
            pagination: {
                el: $('.main-banner__carousel').find('.product-carousel__pagination'),
                clickable: true
            },
            navigation: {
                nextEl: $('.main-banner__carousel').find('.product-carousel__navigation__right'),
                prevEl: $('.main-banner__carousel').find('.product-carousel__navigation__left'),
            }
        });
    }
    
    if ($('.main-section-feedbacks').length) {
        const swiper = new Swiper($('.main-section-feedbacks').find('.swiper-container'), {
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            centeredSlides: false,
            slidesPerGroup: 1,
            spaceBetween: 20,
            speed: 800,
            pagination: {
                el: $('.main-section-feedbacks').find('.product-carousel__pagination'),
                clickable: true
            },
            navigation: {
                nextEl: $('.main-section-feedbacks').find('.product-carousel__navigation__right'),
                prevEl: $('.main-section-feedbacks').find('.product-carousel__navigation__left'),
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                1280: {
                    slidesPerView: 4,
                    spaceBetween: 30
                },
                1600: {
                    slidesPerView: 4,
                    spaceBetween: 40
                }
            },
        });
    }
}());