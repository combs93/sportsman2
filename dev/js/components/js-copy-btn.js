(function () {
    $('.btn-copy').on('click', function() {
        $(this).addClass('is-copied');
        setTimeout(() => {
            $(this).removeClass('is-copied');
        }, 1200);
    });
}());