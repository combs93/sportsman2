(function () {
    $('.js-confirm-modal-trigger').on('click', function() {
        const $this = $(this);
        const $parentForm = $this.closest('.acc-checkout-form');

        $(this).closest('.acc-checkout-form').addClass('is-modal-active');
        $('body, html').animate({scrollTop: $parentForm.offset().top - 150}, 1000);
    });
    
    $('.js-confirm-modal-close').on('click', function() {
        $(this).closest('.acc-checkout-form').removeClass('is-modal-active');
    });

    $('.js-see-products').on('click', function() {
        const idSection = $(this).attr('href'),
            section = $(idSection).offset().top - 85;
        $('body, html').animate({scrollTop: section - 100}, 1500);
    });

    $('#invoice_form').on('submit', function() {
        const $this = $(this);

        // if success
        $this.addClass('is-submitted');
    });
}());