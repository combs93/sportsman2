(function () {
    $('.header__main__list__trigger').on('click', function() {
        if ($(window).width() > 1280) {
            $(this).toggleClass('is-active')
            $('.header__dropdown').fadeToggle(200).toggleClass('is-active');
            $('body').toggleClass('header-dropdown-open');
        }
    });


    $(window).on('load', function() {
        if ($('.header__mobile__list').find('li.is-active').length) {
            const $windowWidth = $(window).width();
            const $activeItem = $('.header__mobile__list').find('li.is-active');
            const $activeItemOffset = $activeItem[0].offsetLeft

            $('.header__mobile__list-wrap').scrollbar();
            $('.header__mobile__list-wrap').scrollLeft($activeItemOffset - $windowWidth / 2 + 50);
            if ($activeItemOffset > 10) {
                $('.header__mobile').addClass('is-scrolled')
            }   
        }
        
        $('.header__mobile__list-wrap').on('scroll', function() {
            if ($(this)[0].scrollLeft < 1) {
                $('.header__mobile').removeClass('is-scrolled')
            } else {
                $('.header__mobile').addClass('is-scrolled')
            }
        });
    });


    $('.js-menu-trigger').on('click', function() {
        $('.header.search-active').removeClass('search-active');
        $('.js-search-input').val('');
        if ($('body').hasClass('menu-is-active')) {
            $('body').removeClass('menu-is-active');
            closeDialog();
        } else {
            $('body').addClass('menu-is-active');
            showDialog();
        }
        setMobileMenuStyles();
    });
    
    function setMobileMenuStyles() {
        if ($('.header').hasClass('header--noty-active')) {
            $('.header__menu').css({
                'top': $('.header__noty').outerHeight() + $('.header__mobile__main').outerHeight(),
                'height': `calc(100vh - ${($('.header__noty').outerHeight() + $('.header__mobile__main').outerHeight())}`
            });
        }
    }

    $('.js-noty-close').on('click', function() {
        $('.header').removeClass('header--noty-active');
        $(this).closest('.header__noty').remove();
        $('.header__menu').css({
            'top': $('.header__mobile__main').outerHeight(),
            'height': '100% - ' + $('.header__mobile__main').outerHeight()
        });
    });

    $(window).on('resize', setMobileMenuStyles);
}());