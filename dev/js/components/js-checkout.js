(function () {
    $('.js-discount-coins-apply').on('click', function() {
        const $this = $(this);
        const $parentRow = $this.closest('.acc-checkout-form__discount__row');
        const $input = $parentRow.find('.form-control');

        if ($input.val().length < 1 || $input.val() < 1) {
            return;
        }

        $parentRow.addClass('is-applied');
        $input.attr('readonly', '');
    });

    $('.js-discount-coins-change').on('click', function() {
        const $this = $(this);
        const $parentRow = $this.closest('.acc-checkout-form__discount__row');
        const $input = $parentRow.find('.form-control');
        
        $parentRow.removeClass('is-applied');
        $input.removeAttr('readonly');
    });
    
    $('.acc-checkout-form__aside').on('click', 'button.article-title__item', function() {
        const $this = $(this);

        $this.toggleClass('is-active');
        $this.closest('.acc-checkout-form__aside').find('.acc-checkout-form__aside__items').slideToggle(400);
    });

    $('.acc-checkout-form__crypto__input').on('change', function() {
        const $this = $(this);
        const $id = $this.attr('id');
        const $parent = $this.closest('.acc-checkout-form__invoice__btc');

        $parent.find('.acc-checkout-form__invoice__btc__content.is-active').hide().removeClass('is-active');
        $parent.find(`.acc-checkout-form__invoice__btc__content[data-info="${$id}"]`).fadeIn(300).addClass('is-active');
    });
}());