(function () {
    $('.collapse-item').on('show.bs.collapse', function() {
        $(this).addClass('is-shown')
    });
    $('.collapse-item').on('hide.bs.collapse', function() {
        $(this).removeClass('is-shown')
    });
}());