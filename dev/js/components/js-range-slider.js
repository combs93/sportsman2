(function () {
    // range slider
    $(document).ready(function() {
        if ($('.js-range-slider').length) {
            $('.js-range-slider').ionRangeSlider({
                skin: 'round',
                min: 0,
                max: 100,
                from: 20,
                grid: false,
                onStart: function (data) {
                },
                onChange: function (data) {
                    $('.js-range-trigger').val(data.from)
                    const $average = $('.refferal-earn__inner__radio__input:checked').siblings('input').val();
                    countMonthlyIncome(data.from, $average);
                    countAnnualIncome(data.from, $average);
                },
            });
            
            const rangeSlider = $('.js-range-slider').data("ionRangeSlider")
            
            $('.js-range-trigger').on('input', function() {
                if ($(this).val() > 100) {
                    $(this).val(100);
                }
                const $invited = $(this).val();
                const $average = $('.refferal-earn__inner__radio__input:checked').siblings('input').val();
                rangeSlider.update({
                    from: $(this).val()
                });
                countMonthlyIncome($invited, $average);
                countAnnualIncome($invited, $average);
            });

            $('.refferal-earn__inner__radio__input').on('change', function() {
                const $average = $(this).siblings('input').val();
                const $invited = $(this).closest('.refferal-earn__inner__count').find('.js-range-trigger').val();
                countMonthlyIncome($invited, $average);
                countAnnualIncome($invited, $average);
            });

            function countMonthlyIncome(invited, average) {
                let $count = invited * average * 0.15;
                $('.js-montly-refferal').text($count);
            }
            
            function countAnnualIncome(invited, average) {
                let $count = invited * average * 12 * 0.15;
                $('.js-annual-refferal').text($count);
            }

            function calculateIncome() {
                const $average = $('.refferal-earn__inner__radio__input:checked').siblings('input').val();
                const $invited = $('.js-range-trigger').val();
                countMonthlyIncome($invited, $average);
                countAnnualIncome($invited, $average);
            }

            calculateIncome();

        }
    });
}());