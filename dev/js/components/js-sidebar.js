(function () {
    if ($('.acc-sidebar').length) {
        let initedStickySidebar = false;
        let stickySidebar;
        
        function initStickySidebar() {
            stickySidebar = new StickySidebar('.acc-sidebar', {
                containerSelector: '.acc-wrapper',
                innerWrapperSelector: '.acc-sidebar__inner',
                topSpacing: $('.header').hasClass('header--noty-active') ? 192 : 142,
                bottomSpacing: 20,
                resizeSensor: true
            });
        }
        
        function destroyStickySidebar() {
            if ($(window).width() < 1280) {
                if (initedStickySidebar) {
                    stickySidebar.destroy();
                    initedStickySidebar = false;
                }
            } else if ($(window).width() > 1279) {
                if (!initedStickySidebar) {
                    initStickySidebar();
                    initedStickySidebar = true;
                }
            }
        }
    
        $(window).on('load resize', function() {
            destroyStickySidebar()
        })
    }

    $('.js-report-problem').on('click', function() {
        const $this = $(this);
        const $parent = $this.closest('.report-info-inner');

        $parent.hide();
        $parent.siblings('.report-action-inner').fadeIn(300);
    });
}());