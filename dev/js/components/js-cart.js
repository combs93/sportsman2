(function () {
    // count price on page load
    $(document).ready(function() {
        countProductsPrice();
        countSubtotalPrice();
        countTotalPrice();
        countQuantity();

        if ($('.my-cart-table').length) {
            $('.my-cart-table-row').each(function() {
                if ($(this).find('.goods-item__count__input').val() > 0) {
                    $(this).addClass('is-filled');
                }
            });
        }
    });

    function countProductsPrice() {
        $('.row--products').each(function() {
            const $this = $(this);
            const $productPrice = parseFloat($this.find('.my-cart-table__price__value').text());
            const $productQuantity = $this.find('.goods-item__count__input').val();
            const $productCountedPrice = $productPrice * $productQuantity;
            $this.find('.my-cart-table__total__value').text($productCountedPrice.toFixed(2));
        });
    }

    function countSubtotalPrice() {
        let $subtotalPrice = 0;
        $('.row--products').each(function() {
            const $this = $(this);
            $subtotalPrice += parseFloat($this.find('.my-cart-table__total__value').text());
            $('.my-cart-table__total__subtotal').text($subtotalPrice.toFixed(2));
        });
    }

    function countTotalPrice() {
        let $totalPrice;
        const $shippingPrice = parseFloat($('.my-cart-table__total__shipping').text());
        const $discountPrice = parseFloat($('.my-cart-table__total__discount').text());
        const $coinsPrice = parseFloat($('.my-cart-table__total__coins').text());
        let $subTotalPrice = parseFloat($('.my-cart-table__total__subtotal').text());
        $totalPrice = $shippingPrice + $discountPrice + $coinsPrice + $subTotalPrice;
        $('.my-cart-table__finally-price__value').text($totalPrice.toFixed(2));
    }

    function countQuantity() {
        let $quantity = 0;
        $('.row--products').each(function() {
            const $this = $(this);
            $quantity += parseInt($this.find('.goods-item__count__input').val());
            $('.my-cart-table__price__count').text($quantity);
        });
    }

    $('.my-cart-table-row').find('.goods-item__count__input').on('input change', function() {
        const parent = $(this).closest('.my-cart-table-row');
        let priceVal = parseFloat(parent.find('.my-cart-table__price__value').text()) * $(this).val();
        parent.find('.my-cart-table__total__value').text(priceVal.toFixed(2));
        countSubtotalPrice();
        countQuantity();
        countTotalPrice();
    });

    // remove product
    $('.js-product-remove').on('click', function() {
        $(this).closest('.row--products').remove();
        if ($('.row--products').length < 1) {
            $('.my-cart-table__total__subtotal').text(0);
            $('.my-cart-table__price__count').text(0);
        }
        countSubtotalPrice();
        countQuantity();
        countTotalPrice();
    });
    
    $('.js-product-erase').on('click', function() {
        const $this = $(this);
        const $parentRow = $this.closest('.my-cart-table-row');

        $parentRow.find('.js-num-input').val(0);
        $parentRow.removeClass('is-filled');
        $parentRow.find('.my-cart-table__total__value').text('0');
        
        countSubtotalPrice();
        countQuantity();
        countTotalPrice();
    });

}());