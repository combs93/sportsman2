(function () {
    $('.products-filters__box').on('change', 'input', function() {
        const $this = $(this);
        const $parentBox = $this.closest('.products-filters__box');
        
        if ($parentBox.find('input:checked').length > 0) {
            $parentBox.addClass('is-marked');
        } else {
            $parentBox.removeClass('is-marked');
        }
    });

    $('.products-filters__box').on('reset', function() {
        $(this).removeClass('is-marked');
    });

    $('.products-filters__box').on('click', '.js-mark-all', function() {
        const $this = $(this);
        const $parentBox = $this.closest('.products-filters__box');

        $parentBox.find('input').prop('checked', true);
        $parentBox.addClass('is-marked');
    });

    if ($('.products-filters').length) {
        let initedStickyFilters = false;
        let stickyFilters;
        
        function initStickySidebar() {
            stickyFilters = new StickySidebar('.products-aside', {
                containerSelector: '.products-wrapper',
                innerWrapperSelector: '.products-filters',
                topSpacing: $('.header').hasClass('header--noty-active') ? 192 : 142,
                bottomSpacing: 30,
                resizeSensor: true
            });
        }
        
        function destroyStickySidebar() {
            if ($(window).width() < 1280) {
                if (initedStickyFilters) {
                    stickyFilters.destroy();
                    initedStickyFilters = false;
                }
            } else if ($(window).width() > 1279) {
                if (!initedStickyFilters) {
                    initStickySidebar();
                    initedStickyFilters = true;
                }
            }
        }
    
        $(window).on('load resize', function() {
            destroyStickySidebar()
        })
    }

    $('.js-filters-trigger').on('click', function() {
        $('.products-aside').addClass('is-opened');
        showDialog();
    });

    $('.js-products-filters-close').on('click', function() {
        $('.products-aside').removeClass('is-opened');

        closeDialog();
    });

    // products view
    $('.js-products-grid-view').on('click', function() {
        const $this = $(this);
        if ($this.hasClass('is-active')) {
            return;
        }
        $this.siblings('.products-inner__view__btn').removeClass('is-active');
        $this.addClass('is-active');
        $this.closest('.products-inner').removeClass('products-inner--list');
    });
    
    $('.js-products-list-view').on('click', function() {
        const $this = $(this);
        if ($this.hasClass('is-active')) {
            return;
        }
        $this.siblings('.products-inner__view__btn').removeClass('is-active');
        $this.addClass('is-active');
        $this.closest('.products-inner').addClass('products-inner--list');
    });

    $('.js-flip-product').on('click', function() {
        $(this).closest('.product-block').addClass('is-flipped');
    });
    
    $('.js-flip-back-product').on('click', function() {
        $(this).closest('.product-block').removeClass('is-flipped');
    });
}());