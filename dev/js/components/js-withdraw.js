(function () {
    $('.js-ask-transfer-check').on('click', function() {
        $(this).closest('.partner-program__footer').addClass('is-checking');
    });
    
    $('.js-approve-form-check').on('click', function() {
        $(this).closest('.partner-program__footer').removeClass('is-checking');
        $(this).closest('.partner-program__footer').addClass('is-approved');
    });
    
    $('.js-cancel-form-check').on('click', function() {
        $(this).closest('.partner-program__footer').removeClass('is-checking is-approved');
    });

    $('#withdraw_processing').on('submit', function() {
        // if success
        const $this = $(this);
        $this.closest('.withdraw-funds').find('.withdraw-verify').fadeIn(300).addClass('is-active');
        $this.find('button[type="submit"]').attr('disabled', 'disabled');
        $('body, html').animate({scrollTop: $('#withdraw_verification_section').offset().top - 200}, 1000);
    });  
    
    $('#withdraw_verification').on('submit', function() {
        // if success
        const $this = $(this);
        $this.find('button[type="submit"]').attr('disabled', 'disabled');
        $this.closest('.withdraw-funds').find('.withdraw-success-wrap').fadeIn(300).addClass('is-active')
        $('body, html').animate({scrollTop: $('#withdraw_success_section').offset().top - 200}, 1000);
    });  
}());