(function () {
    $('.js-photo-products-trigger').on('click', function() {
        $('#photo_coins_modal').modal('show');
    });

    $('.photo-coins-modal').on('click', '.products-table__line-label', function() {
        if ($(this).hasClass('is-active')) {
            return;
        }
        
        const $this = $(this);
        const $id = $this.attr('id');
        const $brand = $this.find('.products-table__line-brand').text();
        const $name = $this.find('.products-table__line-name').text();

        let $productItem = '<div class="acc-coins__photo__products__item" id="' + $id + '">'; 
        $productItem += '<div class="acc-coins__photo__products__item-inner">'
        $productItem += '<span class="acc-coins__photo__products__item-title">' + $brand + '</span>';
        $productItem += '<span class="acc-coins__photo__products__item-text">' + $name + '</span>';
        $productItem += '</div>';
        $productItem += '<div class="acc-coins__photo__products__item-icon">';
        $productItem += '<svg class="close-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">'
        $productItem += '<path fill="#dfe1ee" opacity="0.3" d="M19.757 15.998l11.456-11.429c0.502-0.502 0.784-1.182 0.784-1.892s-0.282-1.39-0.784-1.892c-0.502-0.502-1.182-0.783-1.892-0.783s-1.39 0.282-1.892 0.783l-11.429 11.456-11.429-11.456c-0.502-0.502-1.182-0.783-1.892-0.783s-1.39 0.282-1.892 0.783c-0.502 0.502-0.783 1.182-0.783 1.892s0.282 1.39 0.783 1.892l11.456 11.429-11.456 11.429c-0.25 0.248-0.448 0.542-0.583 0.867s-0.205 0.673-0.205 1.025 0.070 0.7 0.205 1.025c0.135 0.325 0.333 0.619 0.583 0.867 0.248 0.25 0.542 0.448 0.867 0.583s0.673 0.205 1.025 0.205c0.352 0 0.7-0.070 1.025-0.205s0.619-0.334 0.867-0.583l11.429-11.456 11.429 11.456c0.248 0.25 0.542 0.448 0.867 0.583s0.673 0.205 1.025 0.205 0.7-0.070 1.025-0.205c0.325-0.135 0.619-0.334 0.867-0.583 0.25-0.248 0.448-0.542 0.583-0.867s0.205-0.673 0.205-1.025-0.070-0.7-0.205-1.025c-0.135-0.325-0.334-0.619-0.583-0.867l-11.456-11.429z"></path>'
        $productItem += '</svg>'
        $productItem += '</div>';
        $productItem += '</div>';

        $('#acc_coins_products').find('.acc-coins__photo__products').append($productItem);
        $this.addClass('is-active');
        $('body').removeClass('modal-active');
        $('#photo_coins_modal').modal('hide');
        closeDialog();
    });

    $('#acc_coins_products').on('submit', function() {
        $('#coins_submit_modal').modal('show')
    });


    $('body').on('click', '.acc-coins__photo__products__item', function() {
        const $this = $(this);
        const $id = $this.attr('id');

        $('#photo_coins_modal').find('#' + $id).removeClass('is-active');
        $this.remove();
    });
}());