(function () {
    $('.js-btn-change').on('click', function() {
        const $this = $(this);

        $this.addClass('is-hidden');
        $this.siblings('.btn').removeClass('is-hidden');
        $this.closest('.acc-settings__form__line').find('input').removeAttr('readonly');
    });

    $('.js-btn-approve').on('click', function() {
        const $this = $(this);
        
        $this.addClass('is-hidden');
        $this.siblings('.btn').removeClass('is-hidden');
        $this.closest('.acc-settings__form__line').find('input').attr('readonly', '');
    });
    
    $('.js-confirm-delete').on('click', function() {
        const $this = $(this);
        
        $this.closest('.acc-settings__form').find('.acc-settings__form__val').html('');
        $this.closest('.acc-settings__form').find('.btn').remove();
    });

    $('.js-cancel-delete').on('click', function() {
        const $this = $(this);
        
        $this.addClass('is-hidden');
        $this.siblings('.js-confirm-delete').addClass('is-hidden');
        $this.siblings('.js-btn-change').removeClass('is-hidden');
    });
}());