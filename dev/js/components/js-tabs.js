(function () {
    // stats tabs
    $('.js-stats-tab').on('click', function() {
        if ($(this).hasClass('is-active')) {
            return;
        }

        const $this = $(this);
        const $tabId = $this.data('tab');

        $this.siblings('.js-stats-tab.is-active').removeClass('is-active');
        $this.addClass('is-active');

        $('.js-stats-tab-content').removeClass('is-active').hide();
        $('#' + $tabId).fadeIn(300).addClass('is-active');
    });
}());